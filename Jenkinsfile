pipeline {
    agent any

    tools {
        ant 'localAnt'
    }

    environment {
        JUNIT_LOG="build/logs/test-results.xml"
        BASE_BRANCH="upstream/${CHANGE_TARGET}"
        EMAIL=credentials('rootnet-email')
    }

    stages {
        stage('PR Diff Static Analysis') {
            when {
                not {
                    anyOf {
                        branch 'master'
                    }
                }
            }
            steps {
                sh "ant -Dbase.branch=${env.BASE_BRANCH} static-analysis"
            }
            post {
                always {
                    recordIssues(
                        enabledForFailure: true,
                        aggregatingResults : true,
                        tools: [
                            [tool: java()],
                            [pattern: 'build/logs/checkstyle.xml',
                            tool: checkStyle()]]
                        )
                }
            }
        }

        stage('Build') {
            steps {
                sh "cp .env.example .env"
                sh "composer install"
                sh "php artisan key:generate"
            }
        }

        stage('Unit Testing') {
            steps {
                sh "vendor/bin/phpunit --log-junit=${env.JUNIT_LOG}"
                script {
                    currentBuild.result = 'FAILURE'
                }
            }
            post {
                always {
                    junit allowEmptyResults: true, testResults: "${env.JUNIT_LOG}"
                }
            }
        }

        stage('deploy') {
            when {
                branch 'master'
            }
            steps {
                echo 'Deployment Steps'
            }
        }
    }
    post {
        always {
            echo 'Finished job.'
                emailext to: "${EMAIL}", attachLog: true, subject: "${env.JOB_NAME} - Build # ${env.BUILD_NUMBER} - ${currentBuild.currentResult}", body: """
                    ${env.JOB_NAME} - Build # ${env.BUILD_NUMBER} - ${currentBuild.currentResult} for Example<br>
                    Build URL: ${env.BUILD_URL}
                 """

        }
        success {
            echo 'Build successful!'
        }
        failure {
            echo 'Build failed!'
        }
        unstable {
            echo 'Build unstable!'
        }
    }
}

